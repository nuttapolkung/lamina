//
//  ViewController.h
//  Lamina
//
//  Created by Nuttapol Wilailuk on 9/13/2558 BE.
//  Copyright (c) 2558 Nuttapol Wilailuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JJTabBarControllerLib.h"

@interface ViewController : JJTabBarController

@property (weak, nonatomic) IBOutlet UINavigationItem *navigation_item;
@property (weak, nonatomic) IBOutlet UIButton *cart_button;
- (IBAction)openMap:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *cart_number;
@property (weak, nonatomic) IBOutlet UILabel *cart_label;

@end

