//
//  AppDelegate.h
//  Lamina
//
//  Created by Nuttapol Wilailuk on 9/13/2558 BE.
//  Copyright (c) 2558 Nuttapol Wilailuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic) NSMutableArray * filmDatas;
@property (strong,nonatomic) NSMutableArray * cart;

@end

