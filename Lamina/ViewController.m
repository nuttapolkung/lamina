//
//  ViewController.m
//  Lamina
//
//  Created by Nuttapol Wilailuk on 9/13/2558 BE.
//  Copyright (c) 2558 Nuttapol Wilailuk. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
@interface ViewController ()

@end

@implementation ViewController
{
    AppDelegate *app;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    [self.navigationController setNavigationBarHidden:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onUpdateCart) name:@"update_cart" object:nil];
    
}

-(void)onUpdateCart
{
    if(app.cart.count ==0)
    {
        [self.cart_number setHidden:YES];
    
    }else
     [self.cart_number setHidden:NO];
    
    [self.cart_label setText:[NSString stringWithFormat:@"%lu",(unsigned long)app.cart.count]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)openMap:(id)sender {
    NSString *addressString = @"http://maps.apple.com/?q=1+Infinite+Loop,+Cupertino,+CA";
    NSURL *url = [NSURL URLWithString:addressString];
    [[UIApplication sharedApplication] openURL:url];
}
@end
