//
//  CustomNavigationMenuViewController.m
//  Lamina
//
//  Created by Nuttapol Wilailuk on 9/13/2558 BE.
//  Copyright (c) 2558 Nuttapol Wilailuk. All rights reserved.
//

#import "CustomNavigationMenuViewController.h"

@interface CustomNavigationMenuViewController ()

@end

@implementation CustomNavigationMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)taped_sale:(id)sender {
    [[[UIAlertView alloc] initWithTitle:@"Taped" message:@"sale" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil] show];
}

- (IBAction)taped_report:(id)sender {
     [[[UIAlertView alloc] initWithTitle:@"Taped" message:@"report" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil] show];
}
@end
