//
//  FlimTypeViewController.m
//  Lamina
//
//  Created by Nuttapol Wilailuk on 9/16/2558 BE.
//  Copyright (c) 2558 Nuttapol Wilailuk. All rights reserved.
//

#import "FlimTypeViewController.h"

@interface FlimTypeViewController ()

@end

@implementation FlimTypeViewController
{
    NSArray * buildingData;
    NSArray *carData;
}



- (void)viewDidLoad {
    [super viewDidLoad];
   [self.navigationController setNavigationBarHidden:YES];
    buildingData  = [NSArray arrayWithObjects:[[FilmData alloc] initWithData:@"LX 40MGSRPS" description:@"การลดความร้อน จากแสงแดด 46%	การลดความร้อน จากสปอตไลท์	58%	แสงส่องผ่าน39% \nการลดรังสี UV 99% การสะท้อนแสง 11% การลดรังสี อินฟราเรด  - %"],
                     [[FilmData alloc] initWithData:@"LX 20MGSRPS" description:@"การลดความร้อน จากแสงแดด 46% การลดความร้อน จากสปอตไลท์ 58% แสงส่องผ่าน	39%\nการลดรังสี UV 99%	การสะท้อนแสง 11% การลดรังสี อินฟราเรด  - %"],
                     [[FilmData alloc] initWithData:@"LX 05MGSRPS" description:@"การลดความร้อน จากแสงแดด 46%	การลดความร้อน จากสปอตไลท์	58%	แสงส่องผ่าน 39%\nการลดรังสี UV 99% การสะท้อนแสง 11%  การลดรังสี อินฟราเรด  - %"],
                     [[FilmData alloc] initWithData:@"LX 40MGSRPS" description:@"การลดความร้อน จากแสงแดด 46%	การลดความร้อน จากสปอตไลท์	58%	แสงส่องผ่าน39% \nการลดรังสี UV 99% การสะท้อนแสง 11% การลดรังสี อินฟราเรด  - %"],
                     [[FilmData alloc] initWithData:@"LX 20MGSRPS" description:@"การลดความร้อน จากแสงแดด 46% การลดความร้อน จากสปอตไลท์ 58% แสงส่องผ่าน	39%\nการลดรังสี UV 99%	การสะท้อนแสง 11% การลดรังสี อินฟราเรด  - %"],
                     [[FilmData alloc] initWithData:@"LX 05MGSRPS" description:@"การลดความร้อน จากแสงแดด 46%	การลดความร้อน จากสปอตไลท์	58%	แสงส่องผ่าน 39%\nการลดรังสี UV 99% การสะท้อนแสง 11%  การลดรังสี อินฟราเรด  - %"],


                     nil];
    
    carData  = [NSArray arrayWithObjects:[[FilmData alloc] initWithData:@"LX 40MGSRPS" description:@"การลดความร้อน จากแสงแดด 46%	การลดความร้อน จากสปอตไลท์	58%	แสงส่องผ่าน39% \nการลดรังสี UV 99% การสะท้อนแสง 11% การลดรังสี อินฟราเรด  - %"],
                [[FilmData alloc] initWithData:@"LX 20MGSRPS" description:@"การลดความร้อน จากแสงแดด 46% การลดความร้อน จากสปอตไลท์ 58% แสงส่องผ่าน	39%\nการลดรังสี UV 99%	การสะท้อนแสง 11% การลดรังสี อินฟราเรด  - %"],
                [[FilmData alloc] initWithData:@"LX 05MGSRPS" description:@"การลดความร้อน จากแสงแดด 46%	การลดความร้อน จากสปอตไลท์	58%	แสงส่องผ่าน 39%\nการลดรังสี UV 99% การสะท้อนแสง 11%  การลดรังสี อินฟราเรด  - %"],
                [[FilmData alloc] initWithData:@"LX 40MGSRPS" description:@"การลดความร้อน จากแสงแดด 46%	การลดความร้อน จากสปอตไลท์	58%	แสงส่องผ่าน39% \nการลดรังสี UV 99% การสะท้อนแสง 11% การลดรังสี อินฟราเรด  - %"],
    [[FilmData alloc] initWithData:@"LX 20MGSRPS" description:@"การลดความร้อน จากแสงแดด 46% การลดความร้อน จากสปอตไลท์ 58% แสงส่องผ่าน	39%\nการลดรังสี UV 99%	การสะท้อนแสง 11% การลดรังสี อินฟราเรด  - %"],
    [[FilmData alloc] initWithData:@"LX 05MGSRPS" description:@"การลดความร้อน จากแสงแดด 46%	การลดความร้อน จากสปอตไลท์	58%	แสงส่องผ่าน 39%\nการลดรังสี UV 99% การสะท้อนแสง 11%  การลดรังสี อินฟราเรด  - %"],
    
                     nil];

    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSLog(@" %@",segue.identifier);
    if([segue.identifier isEqualToString:@"building"])
    {
        app.filmDatas = [[NSMutableArray alloc] initWithArray:buildingData];
    }else {
        app.filmDatas = [[NSMutableArray alloc] initWithArray:carData];
    }
   
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
