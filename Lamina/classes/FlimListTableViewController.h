//
//  FlimListTableViewController.h
//  Lamina
//
//  Created by Nuttapol Wilailuk on 9/16/2558 BE.
//  Copyright (c) 2558 Nuttapol Wilailuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "FilmData.h"
#import "CellTableViewCell.h"
@interface FlimListTableViewController : UITableViewController
- (IBAction)taped:(id)sender;


@end
