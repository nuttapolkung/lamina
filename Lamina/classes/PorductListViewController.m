//
//  PorductListViewController.m
//  Lamina
//
//  Created by Nuttapol Wilailuk on 9/17/2558 BE.
//  Copyright (c) 2558 Nuttapol Wilailuk. All rights reserved.
//

#import "PorductListViewController.h"

@interface PorductListViewController ()

@end

@implementation PorductListViewController
{
    AppDelegate *app;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    app = (AppDelegate*)[UIApplication sharedApplication].delegate;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return app.filmDatas.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 89;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    //    [cell setTag:indexPath.row];
    FilmData *data = [app.filmDatas objectAtIndex:indexPath.row];
    UILabel * title = (UILabel*)[cell viewWithTag:1];
    [title setText:data.title];
    
    UITextView * desc = (UITextView*)[cell viewWithTag:2];
    [desc setText:data.desc];
    
    //    UIButton * add =  (UIButton*)[cell viewWithTag:3];
    cell.data =data;
    //    [add addTarget:self action:@selector(onTapAddCart:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}


- (IBAction)taped:(id)sender {
    UIButton *button = (UIButton*)sender;
    CellTableViewCell *cell =(CellTableViewCell*)[button superview].superview;
    UITextField * amount = (UITextField*)[cell viewWithTag:5];
      UITextField * p = (UITextField*)[cell viewWithTag:6];
    cell.data.amount = [amount.text integerValue];
    cell.data.price =[p.text integerValue] * cell.data.amount;
    
    [app.cart addObject:cell.data];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"update_cart" object:nil];
}


- (IBAction)taped_back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
