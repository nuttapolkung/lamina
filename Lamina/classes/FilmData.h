//
//  FilmData.h
//  Lamina
//
//  Created by Nuttapol Wilailuk on 9/16/2558 BE.
//  Copyright (c) 2558 Nuttapol Wilailuk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FilmData : NSObject

@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *desc;
@property(readwrite,nonatomic) NSUInteger index;

@property(readwrite,nonatomic) NSUInteger amount;
@property(readwrite,nonatomic) NSUInteger price;

-(id)initWithData:(NSString*)title description:(NSString*)desc;

@end
