//
//  CartTableViewController.m
//  Lamina
//
//  Created by Nuttapol Wilailuk on 9/16/2558 BE.
//  Copyright (c) 2558 Nuttapol Wilailuk. All rights reserved.
//

#import "CartTableViewController.h"
#import "AppDelegate.h"
#import "FilmData.h"
#import "CellTableViewCell.h"

@interface CartTableViewController ()

@end

@implementation CartTableViewController
{
    AppDelegate *app;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    [self setPreferredContentSize:CGSizeMake(500, 600)];
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return app.cart.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CellTableViewCell *cell = (CellTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
   
    FilmData *data = [app.cart objectAtIndex:indexPath.row];
    UILabel * title = (UILabel*)[cell viewWithTag:1];
    [title setText:data.title];
    
    UILabel * desc = (UILabel*)[cell viewWithTag:2];
    [desc setText:data.desc];
    
    UITextField * amount   = (UITextField*)[cell viewWithTag:5];
    [amount setText:[NSString stringWithFormat:@"%i",data.amount]];
    
    //    UIButton * add =  (UIButton*)[cell viewWithTag:3];
    cell.data =data;
    data.index = indexPath.row;
    
    return cell;
}

- (IBAction)taped_remove:(id)sender {
    UIButton *button = (UIButton*)sender;
    
    CellTableViewCell *cell =(CellTableViewCell*)[button superview].superview;
    
    
//    [app.cart removeObject:cell.data];
    [app.cart removeObjectAtIndex:cell.data.index];
    
    [self.tableView reloadData];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"update_cart" object:nil];
}
@end
