//
//  CartTableViewController.h
//  Lamina
//
//  Created by Nuttapol Wilailuk on 9/16/2558 BE.
//  Copyright (c) 2558 Nuttapol Wilailuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartTableViewController : UITableViewController
- (IBAction)taped_remove:(id)sender;

@end
