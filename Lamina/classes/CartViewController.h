//
//  CartViewController.h
//  Lamina
//
//  Created by Nuttapol Wilailuk on 9/18/2558 BE.
//  Copyright (c) 2558 Nuttapol Wilailuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
- (IBAction)taped_remove:(id)sender;
@end
