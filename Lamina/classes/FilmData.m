//
//  FilmData.m
//  Lamina
//
//  Created by Nuttapol Wilailuk on 9/16/2558 BE.
//  Copyright (c) 2558 Nuttapol Wilailuk. All rights reserved.
//

#import "FilmData.h"

@implementation FilmData

-(id)initWithData:(NSString *)title description:(NSString *)desc
{
    self = [super init];
    if(self)
    {
        self.title = title;
        self.desc = desc;
    }
    return self;
}

@end
