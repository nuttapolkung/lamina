//
//  CustomNavigationMenuViewController.h
//  Lamina
//
//  Created by Nuttapol Wilailuk on 9/13/2558 BE.
//  Copyright (c) 2558 Nuttapol Wilailuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomNavigationMenuViewController : UIViewController
- (IBAction)taped_sale:(id)sender;
- (IBAction)taped_report:(id)sender;

@end
