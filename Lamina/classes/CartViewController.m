//
//  CartViewController.m
//  Lamina
//
//  Created by Nuttapol Wilailuk on 9/18/2558 BE.
//  Copyright (c) 2558 Nuttapol Wilailuk. All rights reserved.
//

#import "CartViewController.h"
#import "AppDelegate.h"
#import "FilmData.h"
#import "CellTableViewCell.h"
@interface CartViewController ()

@end

@implementation CartViewController
{
     AppDelegate *app;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    [self setPreferredContentSize:CGSizeMake(500, 600)];

    [self updatePrice];
    // Do any additional setup after loading the view.
}

-(void)updatePrice
{
    int sum =0;
    for (int i=0; i<app.cart.count; i++) {
        FilmData *data = [app.cart objectAtIndex:i];
        sum+=data.price;
        
    }
    
    [self.priceLabel setText:[NSString stringWithFormat:@"%i",sum]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return app.cart.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 89;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CellTableViewCell *cell = (CellTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    
    FilmData *data = [app.cart objectAtIndex:indexPath.row];
    UILabel * title = (UILabel*)[cell viewWithTag:1];
    [title setText:data.title];
    
    UILabel * desc = (UILabel*)[cell viewWithTag:2];
    [desc setText:data.desc];
    
    UITextField * amount   = (UITextField*)[cell viewWithTag:5];
    [amount setText:[NSString stringWithFormat:@"%lu",(unsigned long)data.amount]];
    
    //    UIButton * add =  (UIButton*)[cell viewWithTag:3];
    cell.data =data;
    data.index = indexPath.row;
    
    return cell;
}

- (IBAction)taped_remove:(id)sender {
    UIButton *button = (UIButton*)sender;
    
    CellTableViewCell *cell =(CellTableViewCell*)[button superview].superview;
    
    
    //    [app.cart removeObject:cell.data];
    [app.cart removeObjectAtIndex:cell.data.index];
    
    [self.tableView reloadData];
    
     [self updatePrice];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"update_cart" object:nil];
}

@end
