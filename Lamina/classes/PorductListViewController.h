//
//  PorductListViewController.h
//  Lamina
//
//  Created by Nuttapol Wilailuk on 9/17/2558 BE.
//  Copyright (c) 2558 Nuttapol Wilailuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "FilmData.h"
#import "CellTableViewCell.h"

@interface PorductListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
- (IBAction)taped_back:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)taped:(id)sender ;
@end
